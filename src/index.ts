import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DataSource } from "typeorm"
import { CourseFeedback } from './entity/CourseFeedback.entity';


export const AppDataSource = new DataSource({
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "root",
    password: "2357",
    database: "course-feedbacks",
    synchronize: true,
    logging: true,
    entities: [CourseFeedback],
    migrations: [],
})


AppDataSource.initialize().then(() => {}).catch((error) => console.log(error))


const port: number = +(process?.env?.PORT ?? 56202);

(async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(port);
})();




