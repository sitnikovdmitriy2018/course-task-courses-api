import { Entity, Column, PrimaryGeneratedColumn, BaseEntity } from "typeorm"

@Entity({ name: "course_feedback" })
export class CourseFeedback extends BaseEntity {
    @PrimaryGeneratedColumn({ name: "feedback_id", type: "int"})
    feedbackId: number

    @Column({ name: "course_id", type: "int", nullable: false})
    courseId: number

    @Column({ name: "rating", type: "tinyint", nullable: false })
    rating: number

    @Column({ name: "text", type: "text", nullable: true })
    text?: string
}