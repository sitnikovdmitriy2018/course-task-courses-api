import { Controller, Get, Post, Param, Body } from '@nestjs/common';
import { CourseService } from './course.service';


@Controller('course')
export class CourseController {
  constructor(protected readonly service: CourseService) {

  }



  @Get(':code')
  public async getCourse(@Param('code') code: number): Promise<unknown> {
    return this.service.loadCourseData(code);
  }

  @Post(':code/review')
  public async addRewiew(@Param('code') code: number, @Body() reviewDTO: any): Promise<unknown> {
    const rating = parseInt(reviewDTO.rating);
    const text = reviewDTO.text;

    if (rating == undefined || rating < 1 || rating > 10) return {state: 500, description: "Invalid data"};

    return this.service.addReview(code, reviewDTO.rating as number, reviewDTO.text as string);
  }

  @Get(':code/reviews')
  public async getReviews(@Param('code') code: number): Promise<unknown> {
    return this.service.getReviews(code);
  }
}
