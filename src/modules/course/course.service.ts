import { Injectable } from '@nestjs/common';
import { CourseSeason, EducationLevel } from '../../common/types';
import { CourseFeedback } from '../../entity/CourseFeedback.entity';
import { ICourse } from './types'
import * as cheerio from 'cheerio';
import axios from 'axios';
import * as https from "https";

@Injectable()
export class CourseService {
	public loadCourseData(code:number):ICourse {
		//let res: unknown;

		const httpsAgent = new https.Agent({
	      rejectUnauthorized: false,
	    });

		//send request with axios
	    let res:unknown = axios.get('https://my.ukma.edu.ua/course/'+code, { httpsAgent })
	    .then(function (response):ICourse {
	      
	      const $ = cheerio.load(response.data);

	      let result:ICourse = {
	        code: code,
	        name: $("h1").contents().filter(function () { return this.type === "text"; }).text().trim(),
	        facultyName: $('[title="Факультет, який забезпечує викладання дисципліни"]').parent().parent().children().last().text(),
	        departmentName: $('[title="Кафедра, яка забезпечує викладання дисципліни"]').parent().parent().children().last().text(),
	        level: $('th:contains("Освітній рівень")').parent().children().last().text().includes("Бакалавр") ? EducationLevel.BACHELOR : EducationLevel.MASTER,
	        year: 4,
	        seasons: [ ]
	      };



	      const t = $('button:contains("Перегляд анотації")');
	      if (t.length != 0) {
	        result.description = t.parent().children().last().text().trim();
	      }

	      const t1 = $('th:contains("Викладач")');
	      if (t1.length != 0) {
	        result.teacherName = t1.parent().children().last().text().trim();
	      }

	      const t2 = $('th:contains("Деталі по семестрах")').parent().parent().children();
	      for(var i = 2; i < t2.length; i++) {
	      	const t9 = $(t2[i]).children().first().text().trim();
	      	  if (t9 == "Осінь") {
		        result.seasons.push(CourseSeason.AUTUMN);
		      } else if (t9 == "Літо") {
		        result.seasons.push(CourseSeason.SUMMER);
		      } else if (t9 == "Весна") {
		        result.seasons.push(CourseSeason.SPRING);
		      }
	      }



	      const t4 = $('th:contains("Інформація")').not('th:contains("Інформація про запис")').parent().children().last().children();
	      console.log("child:"+$('th:contains("Інформація")').text());
	      for(var i = 0; i < t4.length; i++){
	          const text = $(t4[i]).text().trim(); 
	          if (text.endsWith(' кред.')) {
	            result.creditsAmount = parseFloat(text.substring(0, text.length-6));
	          }

	          if (text.endsWith(' год.')) {
	            result.hoursAmount = parseFloat(text.substring(0, text.length-5));
	          }

	          if (text.endsWith(' рік')) {
	            const t5 = parseInt(text.substring(0, text.length-4));
	            if (t5 == 1) {
	              result.year = 1;
	            } else if (t5 == 2) {
	              result.year = 3;
	            } else if (t5 == 3){
	              result.year = 3;
	            } else {
	              result.year = 4;
	            }
	          }
	      }

	      return result;
	    }).catch(error => console.log(error));

	    return (res as ICourse);
   	}






   	public addReview(courseId:number, rating:number, text?:string) {
   		const entity = CourseFeedback.create({
   			courseId: courseId,
   			rating: rating,
   			text: text
   		});
   		entity.save();
   	}

   	public getReviews(courseId:number): Promise<CourseFeedback[]> {
   		return CourseFeedback.find({
   			    where: {
			        courseId: courseId
			    },
   		});
   	}
}
