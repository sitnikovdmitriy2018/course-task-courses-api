import { Controller, Get, Param } from '@nestjs/common';
import { ScheduleService } from './schedule.service';

@Controller('schedule')
export class ScheduleController {
  constructor(
    protected readonly service: ScheduleService,
  ) {}

  @Get(':year/:season')
  public async getCourse(@Param('year') year: number, @Param('season') season: 'spring' | 'summer' | 'autumn'): Promise<unknown> {
    return this.service.loadSheldule(year, season);
  }
}
