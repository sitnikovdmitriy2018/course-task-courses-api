import { Injectable } from '@nestjs/common';
import { CourseSeason, EducationLevel } from '../../common/types'
import { IScheduleItemShort, IScheduleItem } from './types'
import * as cheerio from 'cheerio';
import axios from 'axios';
import * as https from "https";

@Injectable()
export class ScheduleService {
	public loadSheldule(year: number, season: 'spring' | 'summer' | 'autumn'):IScheduleItem[] {
		const httpsAgent = new https.Agent({
	      rejectUnauthorized: false,
	    });


		const sazseazon = season == 'autumn' ? 1 : season == 'spring' ? 2 : 3;
		const sazyear = season == 'autumn' ? year : year - 1;
		//send request with axios
	    let res:unknown = axios.get('https://my.ukma.edu.ua/schedule/?year='+sazyear+'&season='+sazseazon, { httpsAgent })
	    .then(function (response):IScheduleItem[] {
	      
	      const $ = cheerio.load(response.data);

	      let result:IScheduleItem[] = [];

	      const divs = $('div');
	      for(var i = 2; i < divs.length; i++) {
	      	const div:any = $(divs[i]);

	      	if (!div.attr('class').startsWith('file-content-')) continue;

	      	const infoNode = div.children().last();
			const info = infoNode.text().trim();
			console.log("info:"+info)
			const arr = info.split(' ');
			arr.pop();
			arr.pop();
			const t3 = arr.pop();
			
			var st = div.children().first().text().trim();
			st = st.substring(11, st.length-1);
			const arr1 = st.split(' ');
			st = arr1[0].split('.').reverse().join('-') + ' ' + arr1[1];

			const url: string|undefined = infoNode.attr('href').trim();
			const specialityName = arr.join(" ");
			const updatedAt = st;
			const facultyName = div.parent().parent().parent().parent().parent().parent().parent().parent().children().first().children().first().children().first().text().trim();
			const level = t3.includes('БП')?EducationLevel.BACHELOR:EducationLevel.MASTER;
			const year = t3.includes('1')?1:t3.includes('2')?2:t3.includes('3')?3:4;
			const s = season == 'autumn' ? CourseSeason.AUTUMN : season == 'spring' ? CourseSeason.SPRING : CourseSeason.SUMMER;

			let item:IScheduleItem = {
				url: url == undefined ? '' : url,
			  	updatedAt: updatedAt == undefined ? '' : updatedAt,
			  	facultyName: facultyName == undefined ? '' : facultyName,
			  	specialityName: specialityName == undefined ? '' : specialityName,
			  	level: level == undefined ? EducationLevel.BACHELOR : level,
			  	year: year == undefined ? 1 : year,
			  	season: s == undefined ? CourseSeason.SUMMER : s
			};

			result.push(item as IScheduleItem);
	      }
	      

	      return result;
	    }).catch(error => console.log(error));

	    return (res as IScheduleItem[]);
	}
}
